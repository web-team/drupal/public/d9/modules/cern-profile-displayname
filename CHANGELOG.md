# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.3] - 12/01/2022

- Update deprecated `getUsername()` to `getDisplayName()`.

## [2.1.2] - 25/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.1.1] - 01/10/2021

- Remove SimpleSAMLphp dependency
- Update composer.json

## [2.1.0] - 11/02/2021

- Add composer.json
- Add CHANGELOG
- Update module to be d9 ready (passes d9 readiness scan)

## [2.0.0] - 07/11/2018

- Initialize stable version

## [1.0.0] - 08/05/2018

- Initial implementation
